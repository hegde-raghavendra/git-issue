function createIssue() {
	
    
    if (document.getElementById("fileInput").files.length > 0) {
		var fileInput = $("#fileInput").prop("files")[0];


	    var formData = new FormData();

	    
	    
	    formData.append( "file" , fileInput );

				
		$.ajax({
			
			url : "/uploadFile",
			type : "POST",
			enctype: 'multipart/form-data',
		    processData: false,  
		    contentType: false,
		    cache: false,
			data : formData,

			
			success : function(data, textStatus, jqXHR) {
				
				var description = $('#issueDescription').val() + "\n ## Attachments \n" + data.markdown;

				var gitIssueData = {
						"title" : $('#issueTitle').val(),
						"description" : description,
						"labels" : "test"
				};
				
				callCreateIssueApi(gitIssueData);
			},
		
			error : function(jqXHR, textStatus, errorThrown) {
				if(500 == jqXHR.status) {
					$('#msgBlock').show();
					$('#infoMsg').html("Technical issue while uploading the attachment");
					$('#mainBody').hide();
					
				}
			}
			
		});

    }
    else {
    	
		var gitIssueData = {
				"title" : $('#issueTitle').val(),
				"description" : $('#issueDescription').val(),
				"labels" : "test"
		};
		
		callCreateIssueApi(gitIssueData);
    	
    }
    

}

function callCreateIssueApi(gitIssueData) {
	
	$.ajax({
		
		url : "/createIssue",
		type : "POST",
		data : gitIssueData,
		
		success : function(data, textStatus, jqXHR) {
			$('#msgBlock').show();
			$('#mainBody').hide();
			$('#ouput').attr("href",data.web_url);
			$('#ouput').html(data.web_url);
		},
	
		error : function(jqXHR, textStatus, errorThrown) {
			if(500 == jqXHR.status) {
				$('#msgBlock').show();
				$('#infoMsg').html("Technical issue when creating issue");
				$('#mainBody').hide();

				
			}
		}
		
	});
	
}