package com.rgh.issue.git;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitIssueUpdateApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitIssueUpdateApplication.class, args);
	}

}
