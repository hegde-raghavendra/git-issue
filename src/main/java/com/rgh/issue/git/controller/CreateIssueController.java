package com.rgh.issue.git.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.rgh.issue.git.entity.CreatedIssue;
import com.rgh.issue.git.entity.GitIssue;

@RestController
@RequestMapping("createIssue")
public class CreateIssueController {
	
	@Value("${git.projectId}")
	private String projectId;
	
	@Value("${git.issueToken}")
	private String issueToken;
	
	@PostMapping
	public CreatedIssue createIssue(GitIssue gitIssue) {
		
		String projectIssuePath = "https://gitlab.com/api/v4/projects/%s/issues";

		
		HttpHeaders headers = new HttpHeaders();
		headers.add("PRIVATE-TOKEN", issueToken);
		

		
		HttpEntity<GitIssue> requestEntity
		 = new HttpEntity<>(gitIssue, headers);
		 
		String serverUrl = String.format(projectIssuePath,projectId);
		 
		RestTemplate restTemplate = new RestTemplate();
		
		ResponseEntity<CreatedIssue> response = restTemplate
		  .postForEntity(serverUrl, requestEntity, CreatedIssue.class);
		
		return response.getBody();
		
		
	}
	
}
