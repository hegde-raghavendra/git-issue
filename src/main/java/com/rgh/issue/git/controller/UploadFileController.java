package com.rgh.issue.git.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.rgh.issue.git.entity.UploadedFile;

@RestController
@RequestMapping("uploadFile")
public class UploadFileController {
	
	@Value("${git.projectId}")
	private String projectId;
	
	@Value("${git.issueToken}")
	private String issueToken;
	
	
	@PostMapping
	public UploadedFile uploadFile(@RequestParam("file") MultipartFile file) {
		
		String projectUploadPath = "https://gitlab.com/api/v4/projects/%s/uploads";

		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		headers.set("PRIVATE-TOKEN", issueToken);
		
		MultiValueMap<String, Object> body
		  = new LinkedMultiValueMap<>();
		body.add("file", file.getResource());
		
		HttpEntity<MultiValueMap<String, Object>> requestEntity
		 = new HttpEntity<>(body, headers);
		 
		String serverUrl = String.format(projectUploadPath,projectId);
		 
		RestTemplate restTemplate = new RestTemplate();
		
		ResponseEntity<UploadedFile> response = restTemplate
		  .postForEntity(serverUrl, requestEntity, UploadedFile.class);
		

		System.out.println(response.getBody().getAlt());
		System.out.println(response.getBody().getUrl());
		System.out.println(response.getBody().getMarkdown());
		
		
		return response.getBody();
		
	}
	
	

}
