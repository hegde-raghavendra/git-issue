package com.rgh.issue.git.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping
public class IssueController {
	
	@GetMapping("issue")
	public String issuePageDisplay(Model model) {
		
		model.addAttribute("fromGet", "true");
		return "gitIssue";
		
	}
	

}
