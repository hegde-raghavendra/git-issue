package com.rgh.issue.git.entity;

public class UploadedFile {
	
	private String alt;
	private String url;
	private String markdown;
	
	public UploadedFile() {
		super();
	}

	public UploadedFile(String alt, String url, String markdown) {
		super();
		this.alt = alt;
		this.url = url;
		this.markdown = markdown;
	}

	public String getAlt() {
		return alt;
	}

	public void setAlt(String alt) {
		this.alt = alt;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMarkdown() {
		return markdown;
	}

	public void setMarkdown(String markdown) {
		this.markdown = markdown;
	}



	
}
