package com.rgh.issue.git.entity;

public class GitIssue {
	
	private String title;
	private String description;
	private String labels;
	
	public GitIssue() {
		super();
	}

	public GitIssue(String title, String description, String labels) {
		super();
		this.title = title;
		this.description = description;
		this.labels = labels;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLabels() {
		return labels;
	}

	public void setLabels(String labels) {
		this.labels = labels;
	}
	
	

	
}
